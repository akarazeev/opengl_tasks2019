#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"

#include <iostream>
#include <vector>
#include <complex>

class SampleApplication : public Application
{
public:
    MeshPtr _surface;
    unsigned int n_polygons = 5;
    float a = 3.0;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

        _cameraMover = std::make_shared<FreeCameraMover>();

//        _surface = makeSurface(n_polygons);
        _surface = makeKlein(n_polygons, a);
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));
//        _surface->setPrimitiveType(GL_POINTS);

//        _shader = std::make_shared<ShaderProgram>("592KarazeevData1/shaderNormal.vert", "592KarazeevData1/shader.frag");
//        _shader = std::make_shared<ShaderProgram>("592KarazeevData1/shaderPoint.vert", "592KarazeevData1/shader.frag");
        _shader = std::make_shared<ShaderProgram>("592KarazeevData1/shader_task1.vert", "592KarazeevData1/shader.frag");
    }

    void update() override
    {
        Application::update();
        if (glfwGetKey(this->_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            if (n_polygons < 500) {
                n_polygons += 1;
//                _surface = makeSurface(n_polygons);
                _surface = makeKlein(n_polygons, a);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));
            }
        }
        if (glfwGetKey(this->_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            if (n_polygons > 4) {
                n_polygons -= 1;
//                _surface = makeSurface(n_polygons);
                _surface = makeKlein(n_polygons, a);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));
            }
        }
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }
};

std::complex<float> complex_int_pow(std::complex<float> a, int pow) {
    std::complex<float> tmp = a;
    for (int i = 1; i < pow; i++) {
        tmp *= a;
    }
    return tmp;
}

int main()
{
    SampleApplication app;
    app.start();

//    std::complex<float> first(2.0, 2.0);
//    std::cout << complex_int_pow(first, 2).imag() << '\n';

    return 0;
}
