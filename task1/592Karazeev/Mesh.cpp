#include "Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <fstream>
#include <vector>

glm::vec3 func_klein(float u, float v, float a)
{
    return glm::vec3(
            (a + (cos(v / 2.0) * sin(u)) - (sin(v / 2.0) * sin(2.0 * u))) * cos(v),
            (a + (cos(v / 2.0) * sin(u)) - (sin(v / 2.0) * sin(2.0 * u))) * sin(v),
            (sin(v / 2.0) * sin(u)) + (cos(v / 2.0) * sin(2.0 * u))
            );
}

glm::vec3 normal_klein(float u, float v, float a)
{
    float dxdv = (((-sin(v / 2.0) * sin(u) / 2.0) - (cos(v / 2.0) * sin(2.0 * u) / 2.0)) * cos(v)) -
                    ((a + (cos(v / 2.0) * sin(u)) - (sin(v / 2.0) * sin(2.0 * u))) * sin(v));
    float dydv = (((sin(v / 2.0) * sin(u) / 2.0) - (cos(v / 2.0) * sin(2.0 * u) / 2.0)) * sin(v)) +
                 ((a + (cos(v / 2.0) * sin(u)) - (sin(v / 2.0) * sin(2.0 * u))) * cos(v));
    float dzdv = (cos(v / 2.0) * sin(u) / 2.0) - (sin(v / 2.0) * sin(2.0 * u) / 2);

    float dxdu = ((cos(v / 2.0) * cos(u)) + (2.0 * sin(v / 2.0) * cos(2.0 * u) / 2.0)) * cos(v);
    float dydu = ((cos(v / 2.0) * cos(u)) + (2.0 * sin(v / 2.0) * cos(2.0 * u) / 2.0)) * sin(v);
    float dzdu = (sin(v / 2.0) * cos(u)) + (2.0 * cos(v / 2.0) * cos(2.0 * u));

    glm::vec3 normal_vec = glm::cross(glm::vec3(dxdv, dydv, dzdv), glm::vec3(dxdu, dydu, dzdu));
    normal_vec = glm::normalize(normal_vec);

    return normal_vec;
}

MeshPtr makeKlein(unsigned int N, float a)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    std::ofstream myfile;
    myfile.open ("kek.obj");

    for (unsigned int i = 0; i < N; i++)
    {
        float u = -(float)glm::pi<float>() + 2.0f * (float)glm::pi<float>() * i / N;
        float u1 = -(float)glm::pi<float>() + 2.0f * (float)glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j < N; j++)
        {
            float v = 2.0f * (float)glm::pi<float>() * j / N;
            float v1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N;

            // First.
            glm::vec3 p1 = func_klein(u, v, a);
            glm::vec3 p2 = func_klein(u, v1, a);
            glm::vec3 p3 = func_klein(u1, v, a);

            vertices.push_back(p1);
            vertices.push_back(p2);
            vertices.push_back(p3);

            myfile << "v " << p1.x << ' ' << p1.y << ' ' << p1.z << '\n';
            myfile << "v " << p2.x << ' ' << p2.y << ' ' << p2.z << '\n';
            myfile << "v " << p3.x << ' ' << p3.y << ' ' << p3.z << '\n';

            normals.push_back(normal_klein(u, v, a));
            normals.push_back(normal_klein(u, v1, a));
            normals.push_back(normal_klein(u1, v, a));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - 2 * (float)i / N));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - 2 * (float)(i + 1) / N));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - 2 * (float)i / N));

            // Second.
            glm::vec3 p4 = func_klein(u1, v1, a);

            vertices.push_back(p4);
            vertices.push_back(p2);
            vertices.push_back(p3);

            myfile << "v " << p4.x << ' ' << p4.y << ' ' << p4.z << '\n';

            normals.push_back(normal_klein(u1, v1, a));
            normals.push_back(normal_klein(u, v1, a));
            normals.push_back(normal_klein(u1, v, a));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - 2 * (float)i / N));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - 2 * (float)(i + 1) / N));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - 2 * (float)(i + 1) / N));
        }
    }

    myfile << '\n';

//    for (unsigned int i = 0; i < N * N; i++)
//    {
//        myfile << "f " << i + 1 << ' ' << i + 2 << ' ' << i + 3 << '\n';
//        myfile << "f " << i + 4 << ' ' << i + 2 << ' ' << i + 3 << '\n';
//    }
    for (unsigned int i = 0; i < N * N; i++)
    {
        myfile << "f " << (i * 3) + 1 << ' ' << (i * 3) + 2 << ' ' << (i * 3) + 3  << '\n';
        myfile << "f " << (i * 3) + 4 << ' ' << i * 3 + 2 << ' ' << (i * 3) + 3 << '\n';
    }

    myfile.close();

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Klein is created with " << vertices.size() << " vertices\n";

    return mesh;
}

float func(float x, float y)
{
    return x * sin(y);
}

float func_der_x(float x, float y)
{
    return sin(y);
}

float func_der_y(float x, float y)
{
    return x * cos(y);
}

glm::vec3 calc_normal(float x, float y)
{
    return glm::vec3(func_der_x(x, y), func_der_y(x, y), -1);
}

MeshPtr makeSurface(unsigned int N)
{
    float side_length = 20.0;
    float side_length2 = side_length / 2.0;
    float grid_step = side_length / N;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < N; i++)
    {
        float x = -side_length2 + i * grid_step;
        float x1 = -side_length2 + (i + 1) * grid_step;

        for (unsigned int j = 0; j < N; j++)
        {
            float y = -side_length2 + j * grid_step;
            float y1 = -side_length2 + (j + 1) * grid_step;

            vertices.push_back(glm::vec3(x, y, func(x, y)));
            vertices.push_back(glm::vec3(x, y1, func(x, y1)));
            vertices.push_back(glm::vec3(x1, y, func(x1, y)));
            normals.push_back(calc_normal(x, y));
            normals.push_back(calc_normal(x, y1));
            normals.push_back(calc_normal(x1, y));

            vertices.push_back(glm::vec3(x1, y, func(x1, y)));
            vertices.push_back(glm::vec3(x, y1, func(x, y1)));
            vertices.push_back(glm::vec3(x1, y1, func(x1, y1)));
            normals.push_back(calc_normal(x1, y));
            normals.push_back(calc_normal(x, y1));
            normals.push_back(calc_normal(x1, y1));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeCube(float size)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, size, -size));
    vertices.push_back(glm::vec3(size, size, size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //front 2
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(size, size, -size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //left 1
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(size, -size, size));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //left 2
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, -size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //top 1
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, size, size));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //top 2
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(size, -size, size));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //back 1
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(-size, size, -size));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //back 2
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(-size, -size, -size));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //right 1
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(size, size, size));
    vertices.push_back(glm::vec3(size, size, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //right 2
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(+size, size, -size));
    vertices.push_back(glm::vec3(-size, size, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //bottom 1
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(size, size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //bottom 2
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(-size, -size, -size));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Cube is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeScreenAlignedQuad()
{
    std::vector<glm::vec3> vertices;

    //front 1
    vertices.push_back(glm::vec3(-1.0, 1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, -1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, 1.0, 0.0));

    //front 2
    vertices.push_back(glm::vec3(-1.0, 1.0, 0.0));
    vertices.push_back(glm::vec3(-1.0, -1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, -1.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr makeGroundPlane(float size, float numTiles)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));
    vertices.push_back(glm::vec3(size, size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, numTiles));

    //front 2
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(-size, -size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(-numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr loadFromAIMesh(const aiMesh &assimpMesh) {
	if (!assimpMesh.HasPositions())
	{
		std::cerr << "This demo does not support meshes without positions\n";
		return std::make_shared<Mesh>();
	}

	if (!assimpMesh.HasNormals())
	{
		std::cerr << "This demo does not support meshes without normals\n";
		return std::make_shared<Mesh>();
	}

	if (!assimpMesh.HasTextureCoords(0))
	{
		std::cerr << "Mesh with no texture coords for texture unit 0 can cause strange visualization\n";
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mVertices);

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mNormals);


	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(assimpMesh.mNumVertices);

	// Upload indices.
	DataBufferPtr indexBuf = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

	std::vector<unsigned int> indices(3 * assimpMesh.mNumFaces);
	for (int i = 0; i < assimpMesh.mNumFaces; i++) {
		const aiFace &face = assimpMesh.mFaces[i];
		if (face.mNumIndices != 3)
			continue;
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
	indexBuf->setData(indices.size() * sizeof(unsigned int), indices.data());
	mesh->setIndices(indices.size(), indexBuf);

	// Optional upload texcoords0.
	if (assimpMesh.HasTextureCoords(0)) {
		DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf2->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mTextureCoords[0]);
		mesh->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	}

	std::cout << "Mesh " << assimpMesh.mName.data << " is loaded with " << assimpMesh.mNumVertices << " vertices\n";
	return mesh;
}

MeshPtr loadFromFile(const std::string& filename, int meshIndex)
{
    aiEnableVerboseLogging(true);
    auto stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, nullptr);
    aiAttachLogStream(&stream);

    const struct aiScene* assimpScene = aiImportFile(filename.c_str(),
		    aiProcessPreset_TargetRealtime_MaxQuality);

    if (!assimpScene)
    {
        std::cerr << aiGetErrorString() << std::endl;
        return std::make_shared<Mesh>();
    }

    if (assimpScene->mNumMeshes <= meshIndex)
    {
        std::cerr << "Wrong mesh index " << meshIndex << " for file " << filename << std::endl;
        return std::make_shared<Mesh>();
    }

    auto mesh = loadFromAIMesh(*assimpScene->mMeshes[meshIndex]);

    aiReleaseImport(assimpScene);
    aiDetachAllLogStreams();

    return mesh;
}