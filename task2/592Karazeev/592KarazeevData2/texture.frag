/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light[3];

void main()
{
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

	vec3 color = vec3(0.0);
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	for (int i = 0; i < 3; i++)
	{
		vec3 lightDirCamSpace = normalize(light[i].pos - posCamSpace.xyz); //направление на источник света
		float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
//		color += light[i].La * material.Ka + light[i].Ld * material.Kd * NdotL; //цвет вершины
		color += diffuseColor * (light[i].La + light[i].Ld * NdotL); //цвет вершины

		if (NdotL > 0.0)
		{
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
//			blinnTerm = pow(blinnTerm, material.shininess); //регулируем размер блика
			blinnTerm = pow(blinnTerm, 0.5); //регулируем размер блика

//			color += light[i].Ls * material.Ks * blinnTerm;
			color += light[i].Ls * blinnTerm * 0.1;
//			vec3 color = diffuseColor * (light.La + light.Ld * NdotL);
		}

	}

	fragColor = vec4(color, 1.0); //просто копируем
}
