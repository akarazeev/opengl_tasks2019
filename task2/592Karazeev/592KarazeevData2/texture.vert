/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitangent;

out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord; //текстурные координаты

out VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	mat3 TBN;
} vs_out;

void main()
{
	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
	
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

	vec3 T = normalize(vec3(modelMatrix * vec4(aTangent,   0.0)));
//	vec3 B = normalize(vec3(modelMatrix * vec4(aBitangent, 0.0)));
//	vec3 N = normalize(vec3(modelMatrix * vec4(vertexNormal,    0.0)));
//	mat3 TBN = mat3(T, B, N);
//	vs_out.TBN = mat3(T, B, N);
}
