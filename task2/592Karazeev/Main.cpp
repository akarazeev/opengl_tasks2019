#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "LightInfo.hpp"

#include "Texture.hpp"
#include "Texture.cpp"

#include <iostream>
#include <vector>
#include <sstream>
#include <complex>

constexpr unsigned int LightNum = 3;

class SampleApplication : public Application
{
public:
    MeshPtr _surface;
    MeshPtr _marker; //Маркер для источника света

    unsigned int n_polygons = 5;
    float a = 3.0;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положениями источников света
    LightInfo _light[LightNum];
    float _lr[LightNum] = {3.0, 5.0, 7.0};
    float _phi[LightNum] = {0.0, 0.1, 0.2};
    float _theta[LightNum] = {glm::pi<float>() * 0.25f, glm::pi<float>() * 0.35f, glm::pi<float>() * 0.45f};

    glm::vec3 ambient[LightNum] = {glm::vec3(0.0, 0.0, 0.2),
                                   glm::vec3(0.2, 0.0, 0.0),
                                   glm::vec3(0.0, 0.2, 0.0)};

    glm::vec3 diffuse[LightNum] = {glm::vec3(0.0, 0.0, 0.8),
                                   glm::vec3(0.8, 0.0, 0.0),
                                   glm::vec3(0.0, 0.8, 0.0)};

    glm::vec3 specular[LightNum] = {glm::vec3(1.0, 1.0, 1.0),
                                    glm::vec3(1.0, 1.0, 1.0),
                                    glm::vec3(1.0, 1.0, 1.0)};

    TexturePtr _worldTexture;
    GLuint _sampler;

    glm::vec3 light_position(float phi, float theta, float lr)
    {
        return glm::vec3(glm::cos(phi) * glm::cos(theta), glm::sin(phi) * glm::cos(theta), glm::sin(theta)) * (float)lr;
    }

    void makeScene() override
    {
        Application::makeScene();

        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

        _cameraMover = std::make_shared<FreeCameraMover>();

        _surface = makeKlein(n_polygons, a);
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));

        _marker = makeSphere(0.1f);

        ///=========================================================
        //Инициализация шейдеров
        _shader = std::make_shared<ShaderProgram>("592KarazeevData2/texture.vert", "592KarazeevData2/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("592KarazeevData2/marker.vert", "592KarazeevData2/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        for (int i = 0; i < LightNum; ++i) {
            _light[i].position = light_position(_phi[i], _theta[i], _lr[i]);
            _light[i].ambient = ambient[i];
            _light[i].diffuse = diffuse[i];
            _light[i].specular = specular[i];
        }

        //=========================================================
        //Загрузка и создание текстур
        _worldTexture = loadTexture("592KarazeevData2/images/grass.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void update() override
    {
        Application::update();
        if (glfwGetKey(this->_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            if (n_polygons < 500) {
                n_polygons += 1;
                _surface = makeKlein(n_polygons, a);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));
            }
        }
        if (glfwGetKey(this->_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            if (n_polygons > 4) {
                n_polygons -= 1;
                _surface = makeKlein(n_polygons, a);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -0.5f)));
            }
        }

        //=========================================================
        // Move light sources
        //
        // Green.
        _phi[2] += 0.01;
        _theta[2] += 0.01;
        // Blue.
        _light[0].position = camera_pos + glm::vec3(-2.0f, 0.0f, -1.0f);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light Source"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light[0].ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light[0].diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light[0].specular));

                ImGui::SliderFloat("radius", &_lr[0], 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi[0], 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta[0], 0.0f, glm::pi<float>());

                ImGui::SliderFloat("radius1", &_lr[1], 0.1f, 10.0f);
                ImGui::SliderFloat("phi1", &_phi[1], 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta1", &_theta[1], 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        for (unsigned int i = 0; i < LightNum; i++)
        {
            if (i == 0)
            {
//                _light[0].position = camera_pos + glm::vec3(0.5f, 0.5f, 0.1f);
            }
            else
            {
                _light[i].position = light_position(_phi[i], _theta[i], _lr[i]);
            }

            std::ostringstream str;
            str << "light[" << i << "]";

            glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light[i].position, 1.0));
            _shader->setVec3Uniform(str.str() + ".pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform(str.str() + ".La", _light[i].ambient);
            _shader->setVec3Uniform(str.str() + ".Ld", _light[i].diffuse);
            _shader->setVec3Uniform(str.str() + ".Ls", _light[i].specular);
        }

        GLuint textureUnitForDiffuseTex = 0;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _worldTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _sampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _worldTexture->bind();
        }
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
            _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));

            _surface->draw();
        }

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            for (int i = 0; i < LightNum; ++i) {
                _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light[i].position));
                _markerShader->setVec4Uniform("color", glm::vec4(_light[i].diffuse, 1.0f));
                _marker->draw();
            }
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

std::complex<float> complex_int_pow(std::complex<float> a, int pow) {
    std::complex<float> tmp = a;
    for (int i = 1; i < pow; i++) {
        tmp *= a;
    }
    return tmp;
}

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}
